<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');

Route::get('/getTickets', 'HomeController@getTickets');

Route::get('/checkCache', function(){
    dd(\Illuminate\Support\Facades\Cache::has('ticket.ALA-TSE.2'));
});