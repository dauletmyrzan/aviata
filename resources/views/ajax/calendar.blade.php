<div class="d-flex flex-row flex-wrap">
@foreach($tickets as $date => $flight)
	<div class="border p-3 bg-white">
		<div class="h6">{{ $date }} </div>
		<div class="font-weight-bold">{{ $flight['price'] }} EUR</div>
	</div>
@endforeach
</div>