@extends('layouts.app')

@section('style')
<style>
    #loading{
        display: none;
    }
</style>
@endsection

@section('content')
    <div class="position-relative overflow-hidden p-3 p-md-5 m-md-3">
        <div class="col-md-6 p-lg-5 mx-auto my-5 shadow bg-white">
            <h1 class="display-5 mb-4 text-center">Поиск дешевых авиабилетов</h1>
            <form action="" class="d-flex justify-content-center align-items-end">
                <div class="form-group mr-2">
                    <label>Направление</label>
                    <select name="direction" id="direction" class="form-control">
                        @foreach($directions as $direction)
                            <option value="{{ $direction->code }}" {{ request()->get('direction') == $direction->code ? 'selected' : '' }}>{{ $direction->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label>Месяц</label>
                    <select name="month" id="month" class="form-control">
                        @for($i = 1; $i < 13; $i++)
                        <option value="{{ $i }}">{{ date('F', mktime(0, 0, 0, $i, 10)) }}</option>
                        @endfor
                    </select>
                </div>
                <button type="button" class="btn btn-success btn-round shadow" id="search_tickets">Найти</button>
            </form>
        </div>
        <div class="product-device shadow-sm d-none d-md-block"></div>
        <div id="loading" class="text-success font-weight-bold text-center h4">Загрузка...</div>
        <div class="product-device product-device-2 shadow-sm d-none d-md-block"></div>
    </div>
    <div class="container" id="result"></div>
@endsection

@section('script')
    <script>
        $(document).ready(function(){
            $("#date_range").daterangepicker({
                locale: {
                    format: 'DD/MM/YYYY'
                }
            });
            $("#search_tickets").on('click', function(){
                $.ajax({
                    url: '/getTickets',
                    data: {
                        direction: $("#direction").val(),
                        month: $("#month").val(),
                    },
                    dataType: "json",
                    beforeSend(){
                        $("#loading").show();
                    },
                    success(response){
                        $("#loading").hide();
                        $("#result").html(response['html']);
                    }
                });
            });
        });
    </script>
@endsection