<?php

namespace App\Http\Controllers;

use App\Direction;
use Illuminate\Http\Request;
use App\Jobs\RetrieveTickets;
use Illuminate\Support\Facades\Cache;

class HomeController extends Controller
{
    public function index()
    {
        $directions = Direction::all();
        return view('welcome', compact('directions'));
    }

    public function getTickets(Request $request)
    {
        $direction = $request->get('direction');
        $month = $request->get('month');
        if(Cache::has('tickets.' . $direction . "." . $month))
        {
            $tickets = Cache::get('tickets.' . $direction . "." . $month);
        }
        else
        {
            $job = new RetrieveTickets($direction, $month);
            $this->dispatch($job);
            $tickets = $job->getResponse();
        }
    	return response()->json([
			'success' => true,
			'html' => view('ajax.calendar', compact('tickets'))->render()
		]);
    }
}
