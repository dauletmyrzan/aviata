<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Cache;

class RetrieveTickets implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $direction;
    private $month;
    public $tickets;

    /**
     * Create a new job instance.
     * @param mixed $direction
     * @param mixed $month
     * @return void
     */
    public function __construct($direction, $month)
    {
        $this->direction = $direction;
        $this->month = $month;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $directions = [];
        if(is_array($this->direction))
        {
            $directions = $this->direction;
        }
        else
        {
            $directions[] = $this->direction;
        }
        foreach($directions as $dir)
        {
            if($this->month == 'all')
            {
                for($i = 1; $i < 13; $i++)
                {
                    $this->sendApiRequest($dir, $i);
                }
            }
            else
            {
                $this->sendApiRequest($dir, $this->month);
            }
        }
    }

    public function sendApiRequest(string $direction, int $month)
    {
        $directionArr = explode('-', $direction);
        $flyFrom = $directionArr[0];
        $flyTo = $directionArr[1];

        $params = [
            'fly_from' => $flyFrom,
            'fly_to' => $flyTo,
            'date_from' => date('d/m/Y', strtotime(date('Y') . '-' . $month . '-1')),
            'date_to' => date('d/m/Y', strtotime(date('Y') . '-' . $month . '-' . cal_days_in_month(CAL_GREGORIAN, $month, date('Y')))),
            'partner' => 'picky',
            'v' => 3,
            'sort' => 'date'
        ];
        $client = new \GuzzleHttp\Client();
        $apiRequest = $client->request('GET', 'https://api.skypicker.com/flights?' . http_build_query($params));
        if($apiRequest->getStatusCode() == 200)
        {
            $apiResponse = json_decode($apiRequest->getBody());
            $data = $apiResponse->data ?? null;
            $tickets = [];
            foreach($data as $flight)
            {
                if($this->ticketValid($flight->booking_token))
                {
                    $tickets[date('d/m/Y', $flight->dTime)][] = [
                        'booking_token' => $flight->booking_token,
                        'price' => $flight->price
                    ];
                }
            }
            foreach($tickets as $key => &$flights)
            {
                usort($flights, function($a, $b){
                    return $a['price'] - $b['price'];
                });
                $tickets[$key] = $flights[0];
            }
            unset($flights);
            Cache::put('tickets.' . $direction . "." . $month, $tickets, now()->addHours(24));
            $this->tickets = $tickets;
        }
    }

    /**
     * Проверка валидности билета
     * @param string $booking_token
     * @return boolean
     */
    public function ticketValid(string $booking_token)
    {
        $url = "https://booking-api.skypicker.com/api/v0.1/check_flights?v=2&booking_token=" . $booking_token . "&bnum=1&pnum=1&affily=picky_{market}&currency=EUR&adults=1&children=0&infants=1";
        $client = new \GuzzleHttp\Client();
        $apiRequest = $client->request('GET', $url);
        if($apiRequest->getStatusCode() == 200)
        {
            $apiResponse = json_decode($apiRequest->getBody());
            return $apiResponse->flights_checked;
        }
        else
        {
            return false;
        }
    }

    public function getResponse()
    {
        return $this->tickets;
    }
}
