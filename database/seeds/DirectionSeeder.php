<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DirectionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('directions')->insert([
            [
                'name' => 'Алматы Астана',
                'code' => 'ALA-TSE'
            ],
            [
                'name' => 'Астана Алматы',
                'code' => 'TSE-ALA'
            ],
            [
                'name' => 'Алматы Москва',
                'code' => 'ALA-MOW'
            ],
            [
                'name' => 'Москва Алматы',
                'code' => 'MOW-ALA'
            ],
            [
                'name' => 'Алматы Шымкент',
                'code' => 'ALA-CIT'
            ],
            [
                'name' => 'Шымкент Алматы',
                'code' => 'CIT-ALA'
            ],
            [
                'name' => 'Астана Москва',
                'code' => 'TSE-MOW'
            ],
            [
                'name' => 'Москва Астана',
                'code' => 'MOW-TSE'
            ],
            [
                'name' => 'Астана Санкт-Петербург',
                'code' => 'TSE-LED'
            ],[
                'name' => 'Санкт-Петербург Астана',
                'code' => 'LED-TSE'
            ]
        ]);
    }
}
